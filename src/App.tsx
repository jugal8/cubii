import React from 'react';
import { Routes,Route } from 'react-router';
import './App.css';
import Header from './component/Header';
import ProtectedRoute from './component/ProtectedRoute';
import Dashbaord from './pages/Dashbaord';
import Login from './pages/Login';
import PageNotFound from './pages/PageNotFound';
import User from './pages/User';

function App() {
  return (
    <div className="App">
     <Header/>
     <Routes>
      <Route  path='/' element={<Login/>}/>
      <Route
          path="/dashbaord"
          element={
            <ProtectedRoute>
              <Dashbaord />
            </ProtectedRoute>
          }
        />
         <Route
          path="/user"
          element={
            <ProtectedRoute>
              <User/>
            </ProtectedRoute>
          }
        />
      <Route path='*' element={<PageNotFound/>}/>
     </Routes>
    </div>
  );
}

export default App;
