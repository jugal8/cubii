import { createSlice, PayloadAction } from "@reduxjs/toolkit";

type InitialState ={
  userInfo: {
    profile_image?:string,
    first_name?:string
    email?:string,
    date_of_birth?:string
    country?:string
  }
}
const initialState:InitialState = {
  userInfo:{
    profile_image:'',
    first_name:'',
    email:'',
    date_of_birth:'',
    country:''
  }
};


const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    user: (state, action: PayloadAction<object> ) => {
       state.userInfo = action.payload
    },
  
  }
});

export const {user} = userSlice.actions
export default userSlice.reducer;
