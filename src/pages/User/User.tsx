import { useEffect } from "react";
import axios from "axios";
import classes from "./user.module.css";
import { useAppDispatch, useAppSelector } from "../../features";
import { user } from "../../store/userSlice";

const user_id = localStorage.getItem("id");
const token = localStorage.getItem("token");
const USER_DATA_URL = `${process.env
  .REACT_APP_BASE_URL}/api/v5/users/${user_id}/`;

const User = () => {
  const dispatch = useAppDispatch();
  const { userInfo } = useAppSelector(state => ({
    ...state.auth
  }));

  useEffect(
    () => {
      axios
        .get(USER_DATA_URL, {
          headers: { Authorization: `Token ${token}` }
        })
        .then(res => {
          dispatch(user(res.data.data as object));
        })
        .catch(err => console.log(err));
    },
    [dispatch]
  );

  return (
    <div className={classes.center}>
      <div className={classes.card}>
        <img src={userInfo.profile_image} alt="Avatar" style={{ width: 100 }} />
        <div className={classes.container}>
          <h4>
            name:<b>{userInfo.first_name}</b>
          </h4>

          <h4>
            email:<b>{userInfo.email}</b>
          </h4>
          <p>
            DOB:{userInfo.date_of_birth}
          </p>
          <p>
            country:{userInfo.country}
          </p>
        </div>
      </div>
    </div>
  );
};

export default User;
