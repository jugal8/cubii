import React, { useRef, useState } from "react";
import axios, { AxiosResponse } from "axios";
import classes from "./Login.module.css";
import { useNavigate } from "react-router";

const REACT_APP_BASE_URL='http://devapi.mycubii.com'
const URL = `${REACT_APP_BASE_URL}/api/v5/login/`;

const Login = () => {
  const [isLogin, setIsLogin] = useState<boolean>(true);
  const [Loading, setLoading] = useState<AxiosResponse | boolean | null>(false);
  const [error, setError] = useState<string>("");

  const emailInputRef = useRef() as React.MutableRefObject<HTMLInputElement>;
  const passwordInputRef = useRef() as React.MutableRefObject<HTMLInputElement>;

  const navigate = useNavigate();
  const switchAuthModeHandler = () => {
    setIsLogin((prevState) => !prevState);
  };

  // handle login data
  const submitHandler = (event: React.FormEvent) => {
    event.preventDefault();

    const enteredEmail = emailInputRef.current.value;
    const enteredPassword = passwordInputRef.current.value;

    setLoading(true);

    axios
      .post(URL, {
        email: enteredEmail,
        password: enteredPassword
      })
      .then((res) => {
        setLoading(false);
        console.log(res);
        if (res) {
          localStorage.setItem("token", res?.data.data.token);
          localStorage.setItem("id", res.data.data.id);
        }
        navigate("/dashbaord");
      })
      .catch((err) => setError(err.message));
  };
  
  return (
    <section className={classes.auth}>
      <h1>{isLogin ? "Login" : "Sign Up"}</h1>
      <form onSubmit={submitHandler}>
        <div className={classes.control}>
          <label htmlFor="email">Your Email</label>
          <input type="email" id="email" required ref={emailInputRef} />
        </div>
        <div className={classes.control}>
          <label htmlFor="password">Your Password</label>
          <input
            type="password"
            id="password"
            required
            ref={passwordInputRef}
          />
        </div>
        <div className={classes.actions}>
          {(!Loading || error) && (
            <button>{isLogin ? "Login" : "Create Account"}</button>
          )}
          {Loading && !error && <p>Sending request...</p>}
          <button
            type="button"
            className={classes.toggle}
            onClick={switchAuthModeHandler}
          >
            {isLogin ? "Create new account" : "Login with existing account"}
          </button>
          {error && <h1>{error}</h1>}
        </div>
      </form>
    </section>
  );
};

export default Login;
