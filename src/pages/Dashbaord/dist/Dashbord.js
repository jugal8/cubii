"use strict";
exports.__esModule = true;
exports.options = void 0;
var react_1 = require("react");
var axios_1 = require("axios");
var chart_js_1 = require("chart.js");
var react_chartjs_2_1 = require("react-chartjs-2");
var user_id = localStorage.getItem("id");
var token = localStorage.getItem("token");
var USER_DATA_URL = process.env.REACT_APP_BASE_URL + "/api/v5/users/" + user_id + "/";
chart_js_1.Chart.register(chart_js_1.CategoryScale, chart_js_1.LinearScale, chart_js_1.BarElement, chart_js_1.Title, chart_js_1.Tooltip, chart_js_1.Legend);
exports.options = {
    responsive: true,
    maintainAspectRatio: true,
    plugins: {
        legend: {
            position: "top"
        },
        title: {
            display: true,
            text: "Calories Chart"
        }
    }
};
var Dashbord = function () {
    var _a = react_1.useState(""), startDate = _a[0], setStartDate = _a[1];
    var _b = react_1.useState(""), endDate = _b[0], setEndDate = _b[1];
    var _c = react_1.useState(""), totalCalories = _c[0], setTotalCalories = _c[1];
    var _d = react_1.useState([]), chartData = _d[0], setChartData = _d[1];
    // const [depandancy, setDepandancy] = useState<any>(false);
    //   const [error, setError] = useState(null);
    var store_start_time = [];
    var store_calories = [];
    chartData.map(function (obj) { return (
    // eslint-disable-next-line no-sequences
    store_start_time.push(obj === null || obj === void 0 ? void 0 : obj.start_time.split("T")[0]),
        store_calories.push(obj === null || obj === void 0 ? void 0 : obj.calories),
        console.log("date", store_start_time),
        console.log("cal", store_calories)); });
    var newFilter = {};
    for (var i = 0; i < store_start_time.length; i++) {
        if (newFilter.hasOwnProperty(parseInt(store_start_time[1]))) {
            newFilter[store_start_time[i]] =
                parseInt(newFilter[store_start_time[i]]) + parseInt(store_calories[i]);
            console.log("store_1", newFilter[store_start_time[i]]);
            console.log("Store_2", store_calories[i]);
        }
        else {
            newFilter[store_start_time[i]] = parseInt(store_calories[i]);
        }
    }
    console.log("newdata", newFilter);
    var out = Object.entries(newFilter).map(function (_a) {
        var k = _a[0], v = _a[1];
        return ({ date: k, cal: v });
    });
    console.log('apdjweiocfhwofw', out);
    var data = {
        labels: out.map(function (obj) { return obj.date; }),
        datasets: [
            {
                label: "Calories / Day",
                data: out.map(function (obj) { return obj.cal; }),
                backgroundColor: "rgba(0, 255, 247, 1)"
            }
        ]
    };
    var GetData = function () {
        axios_1["default"]
            .get(USER_DATA_URL + "progress/activity_log/?start_date=" + startDate + "&end_date=" + endDate + "&response_range=daily ", {
            headers: { Authorization: "Token " + token }
        })
            .then(function (res) {
            setChartData(res.data.data);
        })["catch"](function (err) { return console.log(err.message); });
        axios_1["default"]
            .get(USER_DATA_URL + "progress/?start_date=" + startDate + "&end_date=" + endDate + "&response_range=daily ", {
            headers: { Authorization: "Token " + token }
        })
            .then(function (res) {
            setTotalCalories(res.data.data.goals_data.calories);
        })["catch"](function (err) { return console.log(err.message); });
    };
    var handleChange = function (e) {
        if (e.target.name === "startDate") {
            setStartDate(e.target.value);
            console.log(e.target.name);
        }
        if (e.target.name === "endDate") {
            setEndDate(e.target.value);
            console.log(e.target.name);
        }
    };
    return (React.createElement(React.Fragment, null,
        React.createElement("div", null,
            React.createElement("span", null,
                "Start Date:",
                React.createElement("input", { id: "startDate", type: "date", name: "startDate", onChange: handleChange, value: startDate })),
            React.createElement("span", { className: "ms-4" },
                "End Date:",
                React.createElement("input", { id: "endDate", type: "date", name: "endDate", onChange: handleChange, value: endDate })),
            React.createElement("button", { onClick: function () { return GetData(); }, disabled: startDate && endDate ? false : true }, "Get Data")),
        "TotalCalories: ",
        totalCalories,
        React.createElement(react_chartjs_2_1.Bar, { options: exports.options, data: data })));
};
exports["default"] = Dashbord;
