import { useState } from "react";
import axios, { AxiosResponse } from "axios";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
} from "chart.js";
import { Bar } from "react-chartjs-2";
import classes from "./Dashbaord.module.css";
const REACT_APP_BASE_URL='http://devapi.mycubii.com'
const user_id = localStorage.getItem("id");
const token = localStorage.getItem("token");
const USER_DATA_URL = `${REACT_APP_BASE_URL}/api/v5/users/${user_id}/`;

//Define chartjs globaly
ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);

//custom properties for chart
export const options = {
  responsive: true,
  maintainAspectRatio: true,
  plugins: {
    legend: {
      position: "top" as const
    },
    title: {
      display: true,
      text: "Calories Chart"
    }
  }
};

const Dashbaord = () => {
  const [startDate, setStartDate] = useState<string>("");
  const [endDate, setEndDate] = useState<string>("");
  const [totalCalories, setTotalCalories] = useState<AxiosResponse | string>("");
  const [chartData, setChartData] = useState<any>([]);
  const [error, setError] = useState<string | null>(null);
  const [caloriesError, setCaloriesError] = useState<string | null>(null);
  let store_start_time: string[] = [];
  let store_calories: string[] = [];

  chartData.map(
    (obj: any) => (
      // eslint-disable-next-line no-sequences
      store_start_time.push(obj?.start_time.split("T")[0]),
      store_calories.push(obj?.calories)
    )
  );
  let newFilter: any = {};
  for (let i = 0; i < store_start_time.length; i++) {
    if (newFilter.hasOwnProperty(parseInt(store_start_time[1]))) {
      newFilter[store_start_time[i]] =
        parseInt(newFilter[store_start_time[i]]) + parseInt(store_calories[i]);
    } else {
      newFilter[store_start_time[i]] = parseInt(store_calories[i]);
    }
  }

  let out = Object.entries(newFilter).map(([k, v]) => ({ date: k, cal: v }));
  // X and Y axis data for chart
  const data = {
    labels: out.map((obj: any) => obj.date),
    datasets: [
      {
        label: "Calories / Day",
        data: out.map((obj: any) => obj.cal),
        backgroundColor: "rgba(0, 255, 247, 1)"
      }
    ]
  };

  const getData = () => {
    //get user activity data for graph
    axios
      .get(
        `${USER_DATA_URL}progress/activity_log/?start_date=${startDate}&end_date=${endDate}&response_range=daily `,
        {
          headers: { Authorization: `Token ${token}` }
        }
      )
      .then((res) => {
        setChartData(res?.data?.data);
      })
      .catch((err) => setError(err?.message));
    //get total calories
    axios
      .get(
        `${USER_DATA_URL}progress/?start_date=${startDate}&end_date=${endDate}&response_range=daily `,
        {
          headers: { Authorization: `Token ${token}` }
        }
      )
      .then((res) => {
        setTotalCalories(res?.data?.data?.goals_data.calories);
      })
      .catch((err) => setCaloriesError(err?.message));
  };

  //set start date and end date for graph
  const handleChange = (e: any) => {
    if (e.target.name === "startDate") {
      setStartDate(e.target.value);
    }
    if (e.target.name === "endDate") {
      setEndDate(e.target.value);
    }
  };
  return (
    <>
      <div>
        <span>
          Start Date:
          <input
            id="startDate"
            type="date"
            name="startDate"
            onChange={handleChange}
            value={startDate}
          />
        </span>
        <span className="ms-4">
          End Date:
          <input
            id="endDate"
            type="date"
            name="endDate"
            onChange={handleChange}
            value={endDate}
          />
        </span>
        <button
          onClick={() => getData()}
          disabled={startDate && endDate ? false : true}
          className={classes.getbutton}
        >
          Get Data
        </button>
      </div>
      TotalCalories: {totalCalories}
      {caloriesError && { caloriesError }}
      <Bar options={options} data={data} />
      {error && <h1>{error}</h1>}
    </>
  );
};

export default Dashbaord;
