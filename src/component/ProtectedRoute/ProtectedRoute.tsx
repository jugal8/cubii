import { Navigate } from "react-router-dom";
type Child = {
  children: JSX.Element;
};

const ProtectedRoute = ({ children }: Child) => {
 
 if (!localStorage.getItem("token")) {
    return <Navigate to="/" replace />;
  }

  return children;
};

export default ProtectedRoute;
