import React from "react";
import { Link, useNavigate } from "react-router-dom";
import classes from './Header.module.css'


const Header = () => {
  const navigate = useNavigate();

  const handleLogout = () => {
    localStorage.clear();
    navigate("/");
  };

  return (
    <div>
   
      <div className={classes.header}>
         <a href="#default" className={classes.logo}>
          Cubii
        </a> 
        <div className={classes.header_right}>
          <Link  to="/dashbaord">
            DASHBAORD
          </Link>
          <Link to="/user">USER</Link>
          {localStorage.getItem("token")
        ? <Link to="/" onClick={handleLogout}>
            LOGOUT
          </Link>
        : <Link to="/">LOGIN</Link>}
        </div>
      </div>
    </div>
  );
};

export default Header;
